FROM alpine:3.18.4

ARG HELM_VERSION=v3.13.1
ARG KUBECTL_VERSION=v1.28.3

RUN apk add --update --no-cache curl ca-certificates && \
    curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xvz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm && \
    rm -rf linux-amd64 && \
    curl -L -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    mv kubectl /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl && \ 
    apk del curl
    
RUN adduser -D -u 1000 helm

USER helm
