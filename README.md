# Helm docker image

Docker image to run [helm](https://helm.sh/) command line tool.

## Usage

This image is used to install applications on Kubernetes, using Helm:

    $ docker run --rm --entrypoint /usr/bin/helm -v ~/.config/helm:/home/helm/.config/helm -v ~/.cache/helm:/home/helm/.cache/helm -v ~/.kube/config:/home/helm/.kube/config registry.esss.lu.se/ics-docker/helm:latest install mariadb stable/mariadb

For the container to be stateless, you should mount config and cache directories into the container.
To execute commands on the Kubernetes cluster, you need to supply a kubeconfig (i.e. ~/.kube/config) file to the container.
